---
title: Think RTL
layout: default
nav_order: 10
parent: RTL and BiDi Introduction
permalink: /bidi-intro/think-rtl.html
---

# Think RTL

People who grow up seeing RTL scripts don't just read/write, they also
*think* in that direction most of the time. For example, if two pictures
are placed next to each other depicting the old and new state of
something, the old one should go to the right (the first place), and the
new one to the left.

As a rule of thumb, if an application was designed to have a certain UI,
it should be entirely flipped for RTL scripts. The left-side navbar
should become right-side, the "File Edit ..." menu should be "File" on
the very right, followed by "Edit" to its left, etc.

It's a good practice to try to get rid of any left-to-right
preconception. Never think of LTR as "normal" and RTL as "reverse" or
"exceptional". They should be treated equally. Try not to think of
memory addresses, elements of an array, characters of a logical string
being stored from left to right (even though that's how the BiDi spec
and this document denotes them for practical purposes); try to think of
them for example as top-down, or close-far along the Z axis to help
yourself spot the cases where you'd subconsciously take LTR granted as
the default. Or if you'd prefer to still think of them as left-to-right,
think of it as "LTR" within a different namespace, unrelated to the
"LTR" or "RTL" properties in the displayed content's namespace.

Given an overall direction, it's okay to speak of "normal direction"
(content that matches the overall direction) and "foreign (reverse)
direction" (LTR content embedded in an RTL context, or vice versa).

While LTR scripts are solely LTR most of the time, RTL needs to come
hand-in-hand with BiDi, probably there's no such thing as pure RTL text
direction. Numbers are written LTR, that is, in foreign direction. Maybe
that's the only exception if let's say you read a novel \[info needed\].
In the digital world, especially terminal emulation (shell prompt,
commands etc.) it's pretty unlikely for a text to be RTL only, it
probably contains quite a few English words which need to be written
LTR.
