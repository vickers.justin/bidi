---
title: Paragraphs
layout: default
nav_order: 10
parent: The Recommendation
permalink: /recommendation/paragraphs.html
---

# Paragraphs

The BiDi algorithm operates on *paragraphs* of texts.

In terminal emulators, the reasonable definition of paragraphs, hence
the one we will go with, is the segments delimited by explicit newline
characters (I'll sometimes refer to these as *hard newlines*). A
paragraph appears as one or more rows of the emulator.

In the unlikely case that an emulator doesn't yet have this concept, it
will have to introduce and track it for the sake of BiDi.

Most terminal emulators already track which of the lines were terminated
by a hard newline and which by a character wrapping to the next line.
They use this information e.g. for selecting text on double or triple
mouse click, copying the selected text to the clipboard, or when
autodetecting URLs. Emulators that rewrap their contents on resize also
use this very same concept of paragraphs for this operation.

On the *alternate* screen, most apps (or libraries) produce output where
each line ends in a hard newline, hence paragraphs become equivalent to
lines. This is just a convention, not a technical necessity. Even on the
*normal* screen, paragraphs typically consist of a few lines only.
