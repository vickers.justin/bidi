---
title: Feature reporting
layout: default
nav_order: 60
parent: Future Improvement Ideas
permalink: /future-improvement-ideas/feature-reporting.html
---

# Feature reporting

Introduce new terminfo capability/-ies. Introduce escape sequence to
query whether BiDi is supported. Maybe query the current BiDi mode too

For terminfo and querying features, I think we should be able to report
optional sub-features, conformance level, and have versioning (to report
which version of this spec is supported). Either as parameters somehow,
or each as a new feature.
