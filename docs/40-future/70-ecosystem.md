---
title: The bigger ecosystem
layout: default
nav_order: 70
parent: Future Improvement Ideas
permalink: /future-improvement-ideas/ecosystem.html
---

# The bigger ecosystem
{:.no_toc}

These ideas are not about BiDi in terminal emulation per se, but about
the bigger ecosystem.

* TOC
{:toc}

## Explicit mode on alternate screen

The introduction of the implicit mode, the new default, could break the
overall look of certain fullscreen, BiDi-unaware applications when they
are given BiDi data to handle (e.g. a BiDi file is opened in a text
viewer/editor). As a workaround, it might be reasonable to switch to
explicit mode on the alternate screen for certain apps, or maybe for all
apps except certain ones. I've mentioned it earlier as a secondary goal
of explicit mode to provide damage control for BiDi-unaware app, to make
their UI not fall apart.

On the other hand, I don't want the normal and alternate screens to have
different default behaviors, that would be plain bad design. The two
screens should behave identically. (Also keep in mind that not all
fullscreen apps switch to the alternate screen. In some apps it's even
configurable.)

I recommend that *if* it indeed becomes a demand to default to the
explicit mode on the alternate screen for *most* of the BiDi-unaware
apps, it should be described in terminfo by adding the necessary escape
sequences to the ti/te (smcup/rmcup) capabilities, hence tricking
existing apps into automatically requesting this mode.

Exceptions could be made on a per-app basis to keep the implicit mode by
having wrapper scripts that set a different \$TERM.

Applications that wish to use explicit mode (that is, BiDi-aware
applications) should not rely on some "magic" selecting it for them
(e.g. via the terminfo entry), they should explicitly issue the
corresponding escape sequences themselves.

It's unclear to me at this moment whether the solution built up here
will be specified in this doc or elsewhere.

## Some simple utilities

Certain simple utilities might receive a bit of BiDi handling, in order
to produce nice result. A typical example is when the output consists of
columns, and BiDi should be restricted to contents within each column,
not across them.

Examples include a simple `ls` (printing filenames in multiple columns),
`ls -l` (not only filenames but also month names might be RTL, but they
shouldn't form a single RTL run with the day and hour:minute or year in
between), `grep` on multiple files (the output contains the filename and
the matching line too, either one might be RTL or BiDi). These
particular utilities already do coloring if the output goes to a
terminal, so adding some simple BiDi treatment is not necessarily
far-fetched. They wouldn't need to link against a BiDi library or
contain complicated features, all they'd need to do is surround the
potentially foreign direction text (e.g. filenames) in FSI...PDI. Plus
disable autodetecting the overall direction at least for modes that
produce multiple columns such as plain `ls` or `ls -l`; other modes,
such as the one-column `ls -1` might leave the user's choice unchanged.
Maybe even force LTR context for `ls -l`.

Utilities printing filenames (e.g. `ls`, `grep`, `find`, `realpath`
etc.) might introduce a mode where each `/`-separated component is
marked as a separate potentially foreign direction run.

These require terminal emulators to implement *implicit mode level 2*,
that is, remember BiDi control characters.

For utilities that modify the strings they work with (e.g. `cut`,
`sed`), there's no way to keep the desired *logical* behavior and at the
same time always produce a friendly *visual* output. Readability has to
be sacrificed in order to keep the old logical behavior. That is, these
utilities should not be modified.

## Locale

I can't find the overall direction being defined in the locale database.
Shouldn't it be there? Could be important for one-off wrapper scripts in
front of some apps. Would be required if the RLE...PDF+LRM or
LRE...PDF+RLM spillover workaround is used to restore the paragraph
direction rather than FSI...PDI (although really FSI...PDI should be
used in newly written code).

Where do graphical applications take the overall UI direction from, do
graphical toolkits have a built-in list of RTL locales?
